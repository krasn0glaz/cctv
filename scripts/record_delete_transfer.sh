#!/bin/sh
# record_delete_transfer.sh

target_dir="/tmp/cctv"
td_files="$target_dir/*"
NAS_IP="0.0.0.0"
NAS_TARGET_DIR=/root/cctv/

scp -i ~/.ssh/cctv $td_files $NAS_IP:$NAS_TARGET_DIR
rm -rf $td_files
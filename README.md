# CCTV

## Structure
1. Raspberry Pi with Motion software provides a stream.
2. Server runs scripts by crontab.
3. ffmpeg captures __n__ number of streams from __n__ number of cameras and saves it to a file
4. Saved files are displayed on a webpage f.e.

## Setup

### Requirements

#### Raspberry Pi
We are going to make a livestream on RPi. Of course you will need a camera: you can use either Raspberry Pi Webcam (either standard, it is green, or NoIR, it's black) or a USB Webcam.
And of course you will need some software. We are going to use Motion tool to create a livestream.

First of all upgrade your packages:
```bash
sudo apt update
sudo apt upgrade
```
Then you can install the [Motion](http://www.lavrsen.dk/foswiki/bin/view/Motion/WebHome) tool, which makes our Livestream possible.
```bash
sudo apt-get install motion -y
```
If everything has worked so far, the camera can be connected (if not already done).
USB Camera should be vivible in /dev/video*

Copy motion.conf to \~/.motion/ and start it with ```motion``` command.

Stream will be visible on port 8081, files are saved to /tmp/cctv

Use Crontab with cronjob file to run record_delete_transfer.sh periodically. Script will scp files to NAS and delete old records.

#### Server 

Use a webserver and configure it to view either LIVE or recorded previously videos. 